#!/usr/bin/env python
import argparse
import logging
import os
import sys
from urllib.parse import urljoin

import requests

log = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("nickname", type=str, help="User nickname")
    parser.add_argument(
        "--gitlab-api-url", type=str, default=os.environ.get("GITLAB_API_URL")
    )
    parser.add_argument(
        "--gitlab-api-token", default=os.environ.get("GITLAB_API_TOKEN")
    )
    parser.add_argument(
        "--gitlab-project-id", default=os.environ.get("GITLAB_PROJECT_ID")
    )
    parser.add_argument("--log", default="WARNING", help="level of logging messages")

    args = parser.parse_args()

    if args.gitlab_api_url is None:
        parser.error("Please define GITLAB_API_URL environment variable")
    if args.gitlab_api_token is None:
        parser.error("Please define GITLAB_TRIGGER_TOKEN environment variable")
    if args.gitlab_project_id is None:
        parser.error("Please defined GITLAB_PROJECT_ID environment variable")

    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: {}".format(args.log))
    logging.basicConfig(
        format="%(levelname)s:%(name)s:%(asctime)s:%(message)s",
        level=numeric_level,
        stream=sys.stdout,  # Use stderr if script outputs data to stdout.
    )

    trigger_url = (
        args.gitlab_api_url.rstrip("/")
        + f"/projects/{args.gitlab_project_id}/trigger/pipeline"
    )

    log.info("Trigger pipeline with %r", args.nickname)

    # Call trigger
    form_parameters = {"token": args.gitlab_api_token, "ref": "main"}
    if args.nickname:
        form_parameters["variables[USER_NICKNAME]"] = args.nickname
    res = requests.post(trigger_url, form_parameters)
    res.raise_for_status()

    json_body = res.json()
    path = json_body["detailed_status"]["details_path"]
    log.info(f"Triggered pipeline: {urljoin(args.gitlab_api_url, path)}")


if __name__ == "__main__":
    sys.exit(main())
